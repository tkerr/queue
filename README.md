# Queue Template Classes #

Queue container template classes for C++


### FIFO ###

File: `FifoT.h`

The Fifo template class implements a queue structure with first-in, first-out (FIFO) access.

Objects are pushed onto the tail of the queue and removed from the head of the queue.

The FIFO has a fixed size using memory provided by the caller, making it suitable for use in embedded systems.  The user controls how memory is allocated for the queue.
 
 
### PriorityQueue ###

File: `PriorityQueue.h`

The PriorityQueue template class implements a queue structure with priority.

Objects in the queue are ordered in priority order, with the highest priority object at the head of the queue (i.e., removed first).

The priority queue can support either increasing priority or decreasing priority.  If increasing priority, object A has higher priority if A > B. If decreasing priority, object A has higher priority if A < B.

Objects must support inequality operators > (greater-than) and < (less-than).

The priority queue has a fixed size using memory provided by the caller, making it suitable for use in embedded systems. The user controls how memory is allocated for the queue.


### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT

/*****************************************************************************
 * PriorityQueue.h
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * PriorityQueue class definition.
 */

#ifndef _PRIORITY_QUEUE_H_
#define _PRIORITY_QUEUE_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @brief
 * Implements a queue structure with priority.  Objects in the queue are ordered 
 * in priority order, with the highest priority object at the head of the queue
 * (i.e., removed first).
 *
 * The priority queue can support either increasing priority or decreasing 
 * priority.  If increasing priority, object A has higher priority if A > B.
 * If decreasing priority, object A has higher priority if A < B.
 *
 * Objects must support inequality operators > (greater-than) and < (less-than).
 *
 * The priority queue has a fixed size using memory provided by the caller,
 * making it suitable for use in embedded systems.  The user controls how 
 * memory is allocated for the queue.
 */
template <class T>
class PriorityQueue
{
public:

    /**
     * @brief Default constructor.
     */
    PriorityQueue() : 
        m_initialized(false), 
        m_inc(true),
        m_buffer(0),
        m_head(0),
        m_tail(0),
        m_size(0),
        m_count(0)
        {}
    
    /**
     * @brief Initialize the priority queue.
     *
     * Call once during startup to initialize the queue.
     *
     * @param buffer The memory buffer used to hold the objects.  This is provided 
     * by the caller so that they have control over how memory is allocated for it.
     *
     * @param max_objects The maximum number of objects the queue can hold.  Note that
     * the memory buffer provided must be large enough to hold these objects.
     *
     * @param inc True = increasing priority (a > b), false = decreasing priority (a < b)
     *
     * @return True if the priority queue was initialized successfully, false otherwise.
     */
    bool init(T* buffer, unsigned int max_objects, bool inc = true)
    {
        bool ok = false;
        if (!m_initialized)
        {
            if (buffer != 0)
            {
                m_buffer = buffer;
                m_inc = inc;
                m_size = max_objects;
                m_initialized = true;
                clear();
                ok = true;
            }
        }
        return ok;
    }
    
    /**
     * @brief Return the number of empty slots in the queue.
     */
    unsigned int available() {return m_size - m_count;}
    
    /**
     * @brief Return the number objects in the queue.
     */
    unsigned int count() {return m_count;}
    
    /**
     * @brief Clear all objects from the queue.
     */
    void clear()
    {
        m_head  = 0;
        m_tail  = 0;
        m_count = 0;
    }
    
    /**
     * @brief Push an object onto the queue in priority order.
     *
     * @param source Pointer to the source object to be copied into the queue.
     *
     * @return True if successful, false otherwise.
     */
    bool push(const T* source)
    {
        bool ok = false;
        if (m_initialized && (available() > 0))
        {
            // Add to tail of queue.
            m_buffer[m_tail++] = *source;
            if (m_tail >= m_size) m_tail = 0;
            m_count++;
            ok = true;
           
            // Arrange in priority order.
            int a = (m_tail > 0) ? m_tail - 1 : m_size - 1;
            int b = (a > 0) ? a - 1 : m_size - 1;
            
            for (unsigned int i = 1; i < m_count; i++)
            {
                bool must_swap = (m_inc) ? (m_buffer[a] > m_buffer[b]) : (m_buffer[a] < m_buffer[b]);
                if (must_swap)
                {
                    swap(a, b);
                    a = b;
                    b = (a > 0) ? a - 1 : m_size - 1;
                }
                else break;
            }
        }
        return ok;
    }
    
    /**
     * @brief Pop the highest priority object off the queue.
     *
     * @param dest Pointer to the destination memory location to receive the object.
     *
     * @return True if successful, false otherwise.
     */
    bool pop(T* dest)
    {
        bool ok = false;
        if (m_initialized && (m_count > 0))
        {
            // Remove from head of queue.
            *dest = m_buffer[m_head++];
            if (m_head >= m_size) m_head = 0;
            m_count--;
            ok = true;
        }
        return ok;
    }
    
    
    /**
     * @brief Return a pointer to an object in the queue without removing it.
     *
     * @param index The index of the object relative to the highest priority object
     * (0 = highest priority, 1 = next highest priority, etc.)
     *
     * @return Pointer to the object, or zero if not in queue.
     */
    const T* peek(unsigned int index)
    {
        const T* pObj = 0;
        if (m_initialized && (index < m_count))
        {
            unsigned int buffer_idx = m_head;
            while (index > 0)
            {
                buffer_idx++;
                if (buffer_idx >= m_size) buffer_idx = 0;
                index--;
            }
            pObj = &m_buffer[buffer_idx];
        }
        return pObj;
    }

private:
    
    bool m_initialized;      //!< True if object is initialized and memory is allocated
    bool m_inc;              //!< True = increasing priority (a > b), false = decreasing priority (a < b)
    T*   m_buffer;           //!< Buffer of objects in priority order
    T    m_temp;             //!< Temporary object used for swapping
    unsigned int m_head;     //!< Index of queue head
    unsigned int m_tail;     //!< Index of queue tail
    unsigned int m_size;     //!< Maximum number of objects the queue can hold
    unsigned int m_count;    //!< Current number of objects in the queue
    
    /**
     * @brief Swap the position of objects a and b in the queue.
     */
    void swap(int a, int b)
    {
        m_temp = m_buffer[a];
        m_buffer[a] = m_buffer[b];
        m_buffer[b] = m_temp;
    }

};

#endif // _PRIORITY_QUEUE_H_

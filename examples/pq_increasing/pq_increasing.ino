/******************************************************************************
 * pq_increasing.ino
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief PriorityQueue example sketch with increasing priority.
 *
 * This sketch also acts as a unit test of the PriorityQueue class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "PriorityQueue.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD (115200)
#define PQ_SIZE     (10)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
PriorityQueue<long> pq;  //!< The priority queue object

long pq_buf[PQ_SIZE];    //!< The priority queue buffer

 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * check_available()
 **************************************/
void check_available(unsigned int expected)
{
    unsigned int a = pq.available();
    if (a != expected)
    {
        Serial.print("ERROR: available = ");
        Serial.print(a);
        Serial.print(" expected = ");
        Serial.println(expected);
    }
}


/**************************************
 * check_count()
 **************************************/
void check_count(unsigned int expected)
{
    unsigned int a = pq.count();
    if (a != expected)
    {
        Serial.print("ERROR: count = ");
        Serial.print(a);
        Serial.print(" expected = ");
        Serial.println(expected);
    }
}


/**************************************
 * setup()
 **************************************/ 
void setup()
{
    Serial.begin(SERIAL_BAUD);
    delay(1000);
    Serial.println("Priority Queue example sketch for increasing priority");
   
    // Initialization.
    // Higher value objects have higher priority.
    if (!pq.init(pq_buf, PQ_SIZE, true))
    {
        Serial.println("ERROR: Priority Queue init failed");
        while (true) {}
    }
   
    // Sanity checks.
    check_available(PQ_SIZE);
    check_count(0);
   
    long my_element;
    randomSeed(analogRead(A1));
   
    // Add random numbers to the queue.
    Serial.print("Adding "); Serial.print(PQ_SIZE); Serial.println(" elements to the queue:");
    for (int i = 0; i < PQ_SIZE; i++)
    {
        my_element = random(1, 500);
        if (pq.push(&my_element))
        {
            // Successfully pushed the element onto the queue.  Print it.
            Serial.print(my_element); Serial.print(' ');
        }
        else
        {
             // Push was unsuccessful.
           Serial.print("ERROR adding element "); Serial.print(i + 1); Serial.println(" to the queue");
        }
       
        // Sanity checks.
        check_available(PQ_SIZE - (i + 1));
        check_count(i + 1);
    }
    Serial.println();
   
    // Try to add one more number to the queue.
    // It should fail.
    if (pq.push(&my_element))
    {
        Serial.println("ERROR adding element "); Serial.print(PQ_SIZE + 1); Serial.println(" to the queue:");
        Serial.println("push() should have failed but it passed");
    }
   
    // Remove the numbers one at a time.
    // They should be in order from largest to smallest.
    Serial.println("Removing elements in priority order:");
    for (int i = 0; i < PQ_SIZE; i++)
    {
        if (pq.pop(&my_element))
        {
            // Successfully popped the element from the queue.  Print it.
            Serial.print(my_element); Serial.print(' ');
        }
        else
        {
            // Pop was unsuccessful.
            Serial.print("ERROR removing element "); Serial.print(i + 1); Serial.println(" from the queue");
        }
       
        // Sanity checks.
        check_available(i + 1);
        check_count(PQ_SIZE - (i + 1));
    }
    Serial.println();
   
    // Try to remove one more number from the queue.
    // It should fail.
    if (pq.pop(&my_element))
    {
        Serial.println("ERROR removing element "); Serial.print(PQ_SIZE + 1); Serial.println(" from the queue:");
        Serial.println("pop() should have failed but it passed");
    }
   
    // Worst case: Add increasing numbers to the queue.
    Serial.print("Adding "); Serial.print(PQ_SIZE); Serial.println(" elements to the queue:");
    for (int i = 0; i < PQ_SIZE; i++)
    {
        my_element = i;
        if (pq.push(&my_element))
        {
            // Successfully pushed the element onto the queue.  Print it.
            Serial.print(my_element); Serial.print(' ');
        }
        else
        {
            // Push was unsuccessful.
            Serial.print("ERROR adding element "); Serial.print(i + 1); Serial.println(" to the queue");
        }
    }
    Serial.println();
   
    // Remove them one at a time.
    // They should be in reverse order.
    Serial.println("Removing elements in priority order:");
    for (int i = 0; i < PQ_SIZE; i++)
    {
        if (pq.pop(&my_element))
        {
            // Successfully popped the element from the queue.  Print it.
            Serial.print(my_element); Serial.print(' ');
        }
        else
        {
            // Pop was unsuccessful.
            Serial.print("ERROR removing element "); Serial.print(i + 1); Serial.println(" from the queue");
        }
    }
    Serial.println();
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{

}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.
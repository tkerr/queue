/******************************************************************************
 * pq_words.ino
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief PriorityQueue example sketch using a custom Word class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "PriorityQueue.h"
#include "Word.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD (115200)
#define PQ_SIZE     (10)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
PriorityQueue<Word> pq_increasing;  //!< Priority queue with increasing priority
PriorityQueue<Word> pq_decreasing;  //!< Priority queue with decreasing priority

Word pq_inc_buf[PQ_SIZE];           //!< Buffer for pq_increasing
Word pq_dec_buf[PQ_SIZE];           //!< Buffer for pq_decreasing

Word word_array[PQ_SIZE] = 
    {Word("The"), Word("quick"), Word("brown"), Word("fox"), Word("jumped"), 
    Word("over"), Word("the"), Word("lazy"), Word("red"), Word("dog")};

 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * setup()
 **************************************/ 
void setup()
{
    Serial.begin(SERIAL_BAUD);
    delay(1000);
    Serial.println("Priority Queue example sketch using custom Word objects");
   
    // Initialization.
    // Higher value has higher priority.
    if (!pq_increasing.init(pq_inc_buf, PQ_SIZE, true))
    {
        Serial.println("ERROR: Priority Queue pq_increasing init failed");
        while (true) {}
    }
   
    // Initialization.
    // Lower value has higher priority.
    if (!pq_decreasing.init(pq_dec_buf, PQ_SIZE, false))
    {
        Serial.println("ERROR: Priority Queue pq_decreasing init failed");
        while (true) {}
    }
   
    // Add words to the queue.
    Serial.println();
    Serial.print("Adding "); Serial.print(PQ_SIZE); Serial.println(" elements to the queue:");
   
    for (int i = 0; i < PQ_SIZE; i++)
    {
        Serial.print(word_array[i].cstring()); Serial.print(' ');
       
        if (!pq_increasing.push(&word_array[i]))
        {
            Serial.print("ERROR adding element "); Serial.print(i + 1); Serial.println(" to the increasing queue");
        }
       
        if (!pq_decreasing.push(&word_array[i]))
        {
            Serial.print("ERROR adding element "); Serial.print(i + 1); Serial.println(" to the decreasing queue");
        }
    }
    Serial.println();
   
    Word my_element;
   
    // Remove in increasing priority order.
    Serial.println();
    Serial.println("Increasing priority order:");
    for (int i = 0; i < PQ_SIZE; i++)
    {
        if (!pq_increasing.pop(&my_element))
        {
            Serial.print("ERROR removing element "); Serial.print(i + 1); Serial.println(" from the increasing queue");
        }
        Serial.print(my_element.cstring()); Serial.print(' ');
    }
    Serial.println();
   
    // Remove in decreasing priority order.
    Serial.println();
    Serial.println("Decreasing priority order:");
    for (int i = 0; i < PQ_SIZE; i++)
    {
        if (!pq_decreasing.pop(&my_element))
        {
            Serial.print("ERROR removing element "); Serial.print(i + 1); Serial.println(" from the decreasing queue");
        }
        Serial.print(my_element.cstring()); Serial.print(' ');
    }
    Serial.println();
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{

}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.
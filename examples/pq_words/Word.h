/*****************************************************************************
 * Word.h
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * A custom Word class used to illustrate the use of a PriorityQueue class.
 */

#ifndef _WORD_CLASS_H_
#define _WORD_CLASS_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <string.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @brief
 * A simple string-based class used to illustrate the use of a PriorityQueue class.
 *
 * Also illustrates how to design operators for a custom class.
 *
 * A Word is a string that supports comparison operators (<, >, ==, !=).
 */
class Word
{
public:

    /**
     * @brief Default constructor.
     */
    Word();
    
    /**
     * @brief Construct a Word object using a C-string.
     */
    Word(const char* str);
    
    /**
     * @brief Destructor.
     */
    ~Word();
    
    /**
     * @brief Return the C-string value.
     */
    const char* cstring() {return m_cstring;}
    
    /**
     * @brief The greater-than (>) operator.
     */
    bool operator>(const Word& w) {return strcmp(m_cstring, w.m_cstring) > 0;}
    
    /**
     * @brief The less-than (>) operator.
     */
    bool operator<(const Word& w) {return strcmp(m_cstring, w.m_cstring) < 0;}
    
    /**
     * @brief The equal-to (==) operator.
     */
    bool operator==(const Word& w) {return strcmp(m_cstring, w.m_cstring) == 0;}
    
    /**
     * @brief The not-equal-to (!=) operator.
     */
    bool operator!=(const Word& w) {return strcmp(m_cstring, w.m_cstring) != 0;}

private:
    
    char* m_cstring;    //!< The actual word C-string

};

#endif // _WORD_CLASS_H_

/******************************************************************************
 * Word.cpp
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * A custom Word class used to illustrate the use of a PriorityQueue class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <string.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "Word.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * Word::Word
 **************************************/
Word::Word()
{
    m_cstring = 0;
}


/**************************************
 * Word::Word
 **************************************/
Word::Word(const char* str)
{
    unsigned int len = strlen(str);
    if (m_cstring != 0)
    {
        delete[] m_cstring;
    }
    m_cstring = new char [len+1];
    if (m_cstring != 0)
    {
        strcpy(m_cstring, str);
    }
}


/**************************************
 * Word::~Word
 **************************************/
Word::~Word()
{
    if (m_cstring != 0)
    {
        delete[] m_cstring;
        m_cstring = 0;
    }
}


/*****************************************************************************
 * Private functions.
 ******************************************************************************/


// End of file.

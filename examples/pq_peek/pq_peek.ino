/******************************************************************************
 * pq_peek.ino
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief PriorityQueue peek() method example sketch.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "PriorityQueue.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD (115200)
#define PQ_SIZE     (10)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
PriorityQueue<long> pq;  //!< The priority queue object

long pq_buf[PQ_SIZE];    //!< The priority queue buffer

 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * check_available()
 **************************************/
void check_available(unsigned int expected)
{
    unsigned int a = pq.available();
    if (a != expected)
    {
        Serial.print("ERROR: available = ");
        Serial.print(a);
        Serial.print(" expected = ");
        Serial.println(expected);
    }
}


/**************************************
 * check_count()
 **************************************/
void check_count(unsigned int expected)
{
    unsigned int a = pq.count();
    if (a != expected)
    {
        Serial.print("ERROR: count = ");
        Serial.print(a);
        Serial.print(" expected = ");
        Serial.println(expected);
    }
}


/**************************************
 * setup()
 **************************************/ 
void setup()
{
    Serial.begin(SERIAL_BAUD);
    delay(1000);
    Serial.println("Priority Queue example sketch using the peek() method");
   
    // Initialization.
    // Higher value objects have higher priority.
    if (!pq.init(pq_buf, PQ_SIZE, true))
    {
        Serial.println("ERROR: Priority Queue init failed");
        while (true) {}
    }

    long my_element;
    randomSeed(analogRead(A1));
   
    // Add random numbers to the queue.
    Serial.println();
    Serial.print("Adding "); Serial.print(PQ_SIZE); Serial.println(" elements to the queue:");
    for (int i = 0; i < PQ_SIZE; i++)
    {
        my_element = random(1, 500);
        if (pq.push(&my_element))
        {
            // Successfully pushed the element onto the queue.  Print it.
            Serial.print(my_element); Serial.print(' ');
        }
        else
        {
            // Push was unsuccessful.
            Serial.print("ERROR adding element "); Serial.print(i + 1); Serial.println(" to the queue");
        }
    }
    Serial.println();
   
    // Sanity checks.
    check_available(0);
    check_count(PQ_SIZE);
   
    // Take a peek at the elements.
    const long* pObj = 0;
    Serial.println();
    Serial.println("Peeking at the elements in priority order:");
    for (int i = 0; i < PQ_SIZE; i++)
    {
        pObj = pq.peek(i);
        if (pObj != 0)
        {
            // Peek was successful.
            Serial.print(*pObj); Serial.print(' ');
        }
       else
       {
           // Peek was unsuccessful.
           Serial.print("ERROR peeking at element "); Serial.println(i + 1);
       }
    }
    Serial.println();
   
    // Sanity checks.
    check_available(0);
    check_count(PQ_SIZE);
   
    // Test the clear function.
    Serial.println();
    Serial.println("Clearing the priority queue.");
    pq.clear();
   
    // Sanity checks.
    check_available(PQ_SIZE);
    check_count(0);
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{

}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.
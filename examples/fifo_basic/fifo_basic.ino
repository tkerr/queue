/******************************************************************************
 * fifo_basic.ino
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Fifo template class example sketch.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "FifoT.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD (115200)
#define FIFO_SIZE   (10)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
Fifo<long> myFifo;        //!< The FIFO object

long fifo_buf[FIFO_SIZE]; //!< The FIFO buffer

 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * check_available()
 **************************************/
void check_available(unsigned int expected)
{
    unsigned int a = myFifo.available();
    if (a != expected)
    {
        Serial.print("ERROR: available = ");
        Serial.print(a);
        Serial.print(" expected = ");
        Serial.println(expected);
    }
}


/**************************************
 * check_count()
 **************************************/
void check_count(unsigned int expected)
{
    unsigned int a = myFifo.count();
    if (a != expected)
    {
        Serial.print("ERROR: count = ");
        Serial.print(a);
        Serial.print(" expected = ");
        Serial.println(expected);
    }
}


/**************************************
 * setup()
 **************************************/ 
void setup()
{
    Serial.begin(SERIAL_BAUD);
    delay(1000);
    Serial.println("Fifo example sketch");
   
    // Initialization.
    if (!myFifo.init(fifo_buf, FIFO_SIZE))
    {
        Serial.println("ERROR: Fifo init failed");
        while (true) {}
    }
   
    // Sanity checks.
    check_available(FIFO_SIZE);
    check_count(0);
   
    long my_element;
    randomSeed(analogRead(A1));
   
    // Add random numbers to the FIFO.
    Serial.print("Adding "); Serial.print(FIFO_SIZE); Serial.println(" elements to the FIFO:");
    for (int i = 0; i < FIFO_SIZE; i++)
    {
        my_element = random(1, 500);
        if (myFifo.push(&my_element))
        {
            // Successfully pushed the element onto the FIFO.  Print it.
            Serial.print(my_element); Serial.print(' ');
        }
        else
        {
            // Push was unsuccessful.
            Serial.print("ERROR adding element "); Serial.print(i + 1); Serial.println(" to the FIFO");
        }
       
        // Sanity checks.
        check_available(FIFO_SIZE - (i + 1));
        check_count(i + 1);
    }
    Serial.println();
   
    // Remove the numbers one at a time.
    // They should be in the same order as they were added.
    Serial.println("Removing elements in FIFO order:");
    for (int i = 0; i < FIFO_SIZE; i++)
    {
        if (myFifo.pop(&my_element))
        {
            // Successfully popped the element from the FIFO.  Print it.
            Serial.print(my_element); Serial.print(' ');
        }
        else
        {
            // Pop was unsuccessful.
            Serial.print("ERROR removing element "); Serial.print(i + 1); Serial.println(" from the FIFO");
        }
       
        // Sanity checks.
        check_available(i + 1);
        check_count(FIFO_SIZE - (i + 1));
    }
    Serial.println();
   
    // Try to remove one more number from the FIFO.
    // It should fail.
    if (myFifo.pop(&my_element))
    {
        Serial.println("ERROR removing element "); Serial.print(FIFO_SIZE + 1); Serial.println(" from the FIFO:");
        Serial.println("pop() should have failed but it passed");
    }
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{

}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.